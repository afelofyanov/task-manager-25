package ru.tsc.felofyanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.IUserRepository;
import ru.tsc.felofyanov.tm.model.User;

import java.util.Optional;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    @Nullable
    public User findByLogin(@NotNull final String login) {
        return models.stream()
                .filter(user -> login.equals(user.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public User findByEmail(@NotNull final String email) {
        return models.stream()
                .filter(user -> email.equals(user.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public User removeByLogin(@NotNull final String login) {
        @Nullable final Optional<User> model = Optional.ofNullable(findByLogin(login));
        model.ifPresent(models::remove);
        return model.get();
    }
}

package ru.tsc.felofyanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.model.Task;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-list-by-project-id";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show task list by project.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");

        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        @NotNull final List<Task> tasks = getServiceLocator().getTaskService().findAllByProjectId(userId, projectId);
        renderTasks(tasks);
    }
}

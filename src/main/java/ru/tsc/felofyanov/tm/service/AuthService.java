package ru.tsc.felofyanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.service.IAuthService;
import ru.tsc.felofyanov.tm.api.service.IPropertyService;
import ru.tsc.felofyanov.tm.api.service.IUserService;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.exception.entity.UserNotFoundException;
import ru.tsc.felofyanov.tm.exception.field.LoginEmptyException;
import ru.tsc.felofyanov.tm.exception.field.PasswordEmptyException;
import ru.tsc.felofyanov.tm.exception.field.UserIdEmptyException;
import ru.tsc.felofyanov.tm.exception.system.AccessDeniedException;
import ru.tsc.felofyanov.tm.exception.system.AuthenticationException;
import ru.tsc.felofyanov.tm.exception.system.LockException;
import ru.tsc.felofyanov.tm.exception.system.PermissionException;
import ru.tsc.felofyanov.tm.model.User;
import ru.tsc.felofyanov.tm.util.HashUtil;

import java.util.Arrays;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @Nullable
    private String userId;

    public AuthService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IUserService userService
    ) {
        this.propertyService = propertyService;
        this.userService = userService;
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AuthenticationException();
        if (user.getLocked()) throw new LockException();
        if (user.getPasswordHash() == null) throw new AuthenticationException();
        if (!user.getPasswordHash().equals(HashUtil.salt (propertyService, password))) throw new AuthenticationException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    @Nullable
    public User registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        return userService.create(login, password, email);
    }

    @Nullable
    @Override
    public User getUser() {
        if (isAuth()) throw new AccessDeniedException();
        @NotNull final String userId = getUserId();
        return userService.findOneById(userId);
    }

    @NotNull
    @Override
    public String getUserId() {
        if (userId == null) throw new UserIdEmptyException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    public void checkRoles(@Nullable final Role[] roles) {
        if (roles == null) return;
        @Nullable final User user = getUser();
        if (user == null) throw new UserNotFoundException();
        @NotNull final Role role = user.getRole();

        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }
}

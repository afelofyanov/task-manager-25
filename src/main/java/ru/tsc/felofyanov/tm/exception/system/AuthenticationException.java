package ru.tsc.felofyanov.tm.exception.system;

import ru.tsc.felofyanov.tm.exception.AbstractExcception;

public class AuthenticationException extends AbstractExcception {
    public AuthenticationException() {
        super("ERROR! Incorrect login or password entered....");
    }
}
